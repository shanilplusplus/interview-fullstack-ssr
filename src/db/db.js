import { Sequelize } from "sequelize/";
require('dotenv').config();

const db = new Sequelize(process.env.REACT_APP_DB_DATABASE, process.env.REACT_APP_DB_USERNAME,
    process.env.REACT_APP_DB_PASSWORD, {
    host: process.env.REACT_APP_DB_HOST,
    dialect: 'postgres',
    protocol: 'postgres',
    dialectOptions: {
        ssl: {
            require: true,
            rejectUnauthorized: false
        }
    }
});



export default db;