import './App.scss';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Recipes from "./Components/Recipes";
import Recipe from './Components/Recipe';





function App() {
 
  return (
      <Router>
        <Route path="/" exact={true} component={Recipes} />
        <Route path="/recipes" exact={true} component={Recipes} />
        <Route path="/recipes/:id" component={Recipe} />
      </Router>
  );
}

export default App;
