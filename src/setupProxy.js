const { createProxyMiddleware } = require('http-proxy-middleware');




module.exports = function(app) {
    app.use(
      createProxyMiddleware(["/","/recipes"], { target: "http://localhost:5005" })
    );
  };