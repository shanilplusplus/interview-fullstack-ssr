import '../App.scss';
import axios from 'axios';
import React,{useState, useEffect} from 'react';
import { Link } from 'react-router-dom';


const Recipes = () => {

    const [recipeAPIData, setrecipeAPIData] = useState([])

    useEffect(() => {
        getAPIData()
    }, [])


    const getAPIData = async () => {
        
        try{
        const apiData = await axios.get('/recipes')
        console.log(apiData.data, "response")
        setrecipeAPIData(apiData.data)
        } catch(err){
        console.log(err);
        }

    }

    return(
        <div className="recipes-app">
            <h2 className="recipes-app-heading" >Recipes for this Winter</h2>
            <p className="recipes-app-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto labore possimus nemo laudantium obcaecati. </p>
            <div className="recipeItem-container">
                {recipeAPIData && recipeAPIData.map((e) => {
                return(
                    <div className="recipeItem" key={e.id}>
                    
                    <Link to={`recipes/${e.id}`}>
                    <img src={`${e.image}`} alt=""/>
                    <p className="recipeItem-title">{e.title}</p>
                    </Link>
                    </div>
                )
                })}

            </div>

        </div>
    )
}


export default Recipes;