import React,{useState,useEffect} from "react";
import axios from "axios";

const Recipe = ({match}) => {
    const [recipe, setRecipe] = useState();

    useEffect(() => {
        getAPIData()
    }, [])



    const getAPIData = async () => {
        try{
        const apiData = await axios.get(`/recipes/${match.params.id}`)
        console.log(apiData, "response")
        setRecipe(apiData.data)
        } catch(err){
        console.log(err);
        }

    }
    return(
        <div className="recipe">
            <p>{recipe && recipe.title}</p>
            <p>{recipe && recipe.content}</p>
        </div>
    )
    
}


export default Recipe;