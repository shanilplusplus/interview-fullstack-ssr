import {Sequelize} from "sequelize";
import db from "../db/db";


const Recipe = db.define('recipe', {
    title:{
        type: Sequelize.STRING
    },
    author:{
        type: Sequelize.STRING
    },
    content:{
        type: Sequelize.STRING
    },
    image:{
        type: Sequelize.STRING
    },
    createdAt: {
        type: Sequelize.DATE
    },
    updatedAt: {
        type: Sequelize.DATE
    }

})


export default Recipe;