import express from "express"; 

import Recipe from  "../src/models/recipe";
import db from "../src/db/db";

const router = express.Router();




router.get('/', async (req,res) => {
    try{
        const allRecipes = await Recipe.findAll()
        res.json(allRecipes);
    }
    catch(err){
        res.json({error: err})
    }

})

router.get('/:id', async (req,res) => {
    try{
        const recipe = await Recipe.findByPk(req.params.id)
        res.json(recipe);
    }
    catch(err){
        res.json({error: err})
    }

})

router.post('/', async(req,res) => {
    const dummyData ={
        title:"Canadian Crepes",
        author:"Shaun Sigera",
        content: "TThis basic crepe recipe tastes just like the crepes I enjoyed in Canada. It is often said, They taste like the ones my grandmother used to make. They are amazing when filled with a bit of sweetened whipped cream, folded, and then drizzled with maple syrup.",
        image:"https://www.thespruceeats.com/thmb/VTREb8kTu8Q7HVfzS0NFiZylwW0=/960x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/Canadian-crepe-GettyImages-76948196-599252e8054ad90011e4c05f.jpg"
    }

    //destructure 
    const {title, author, content, image} = dummyData;
    // Recipe.create({
    //     title,author,content,image
    // })

    try{
        const createdRecipe = await Recipe.create({
            title,author,content,image
        })
        res.json(createdRecipe);
    }
    catch(err){
        res.json({error: err})
    }
})


export default router; 