import express from "express";
import cors from "cors";
import db from "./src/db/db";
import path from "path";
import recipeRoute from "./routes/recipes";
import React from "react";
import ReactDOMServer from "react-dom/server";
import App from "./src/App";
import fs from "fs";

const app = express();
require('dotenv').config();


app.use(express.static(path.join(__dirname, 'build')));


app.use(cors())

db.authenticate()
    .then(() => console.log('DB connected'))
    .catch(err => console.log(err))


app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.get('/',  (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
  res.json({hi:"hello"})
});

app.use('^/$', (req,res,next) => {
  fs.readFile(path.join(__dirname, 'build', 'index.html'), 'utf-8', (err,data) => {
      if(err){
      console.log(err)
        return res.status(500).send("error")
      }

      return res.send(data.replace(
        '<div id="root"></div>',
        `<div id="root">${ReactDOMServer.renderToString(<App />)}</div>`
      ))
  
})
});
app.use(express.static(path.resolve(__dirname, '..', 'build')))
app.use('/recipes', recipeRoute)

app.listen(process.env.port || 5005, () => {
    console.log("server fired up")
})