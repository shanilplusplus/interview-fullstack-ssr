# FS Interview #12
## Task
Render React elements from an api.  

## subtaks  
        Create Api routes use express  
        created db schema and seed from the db. use pg,prisma  
        render elements on the view layer from seeded db with minimal styling.  
        react elements should be visible when the '/' gets curled.  

### Prerequisites

you need to have node installed in your computer. 


## Getting Started

to get started on you local machine, clone this repo, and install all the node modules. 




### Installing
cd into the folder  
install npm dependencies  


### Running Locally

fire up the server with
```
npm run dev
```

or 

fire up the SSR with
```
npm run ssr
```


fire up cra with

```
npm run start
```



### View Demo
[fullstack-ssr](https://interview-fullstack-ssr.herokuapp.com/)

## Built With

* [React.js](https://reactjs.org/) - for React and ReactDom libraries
* [express](https://github.com/expressjs/express) - Server
* [postgres](https://github.com/porsager/postgres) - Database
* [sequelize](https://github.com/sequelize/sequelize) - ORM
* [babel](https://www.npmjs.com/package/Babel) - compiling and transpling
* [React router](https://github.com/remix-run/react-router) - Routing
* [sass](https://github.com/sass/node-sass) - for scss


## Authors

* **Shaun Sigera** (http://sigera.ca/)


## License

This project is licensed under the MIT License


